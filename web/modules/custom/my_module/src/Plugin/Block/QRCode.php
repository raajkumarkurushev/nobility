<?php

namespace Drupal\my_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\my_module\Response\QRImageResponse;
use Drupal\Core\Url;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "qr_code",
 *   admin_label = @Translation("Product QR Code"),
 * )
 */
class QRCode extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    
    $result = '';
    if($node) {
      $type = $node->bundle();
      
      if($type == 'products') {
        $productUrl = $node->toUrl('canonical', ['absolute' => true])->toString();
        $option = [
          'query' => ['path' => $productUrl],
        ];
        $uri = Url::fromRoute('my_module.generate_qr_code.url', [], $option)->toString();
        
        $result = [];
        $result['qr_scan'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Scan here on your mobile'),
        ];
        
        $result['qr_scan']['description'] = [
          '#markup' => $this->t('To purchase this product on your app to avail exclusive app-only'),
        ];
        
        $result['qr_scan']['qr_code'] = [
          '#theme' => 'image',
          '#uri' => $uri,
          '#attributes' => ['class' => 'module-name-center-image'],
        ];
        
        return $result;
      }
    }
    
    return [
      '#markup' => $result,
    ];
  }
}